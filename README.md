# gitlab-pipeline-trace

Tool for ad-hoc ci pipeline importing. Similar to [release-tools pipeline tracer](https://gitlab.com/gitlab-org/release-tools/-/tree/master/lib/release_tools/pipeline_tracer), but slightly more standalone.

### Install

```shell
$ go install gitlab.com/gitlab-com/gl-infra/gitlab-pipeline-trace@latest
```

## Setup

### Configure glab

This tool reads tokens from the [glab](https://gitlab.com/gitlab-org/cli) config file in `~/.config/glab-cli/config.yml`.

Alternatively, you can provide the token via the `GITLAB_TOKEN` environment variable.

### Setup jaeger

This tool writes traces to a local otel trace collector via otlp. Jaeger supports this protocol, and listens on port `4317`. Jaeger itself is accessible at `http://localhost:16686`.

Download, install, and run the jaeger all-in-one package: https://www.jaegertracing.io/download/.

Then point `gitlab-pipeline-trace` at this jaeger instance (the default is https, this is needed to get http):

```
export OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:4318
```

## Usage

You can import pipelines, list pipelines, and use that list for bulk imports.

```shell
# import a single pipeline
$ gitlab-pipeline-trace import https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/pipelines/1199729880

# list recent completed pipelines
$ gitlab-pipeline-trace list https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist

# import recent pipelines
$ gitlab-pipeline-trace list --limit 1000 https://ops.gitlab.net/gitlab-org/release/tools | jq -r 'select(.name|startswith("Deployment pipeline - "))|.web_url' | parallel -j1 gitlab-pipeline-trace import
```

### Sending to GitLab GOB

Traces can be sent to [GitLab Observability Tracing](https://docs.gitlab.com/ee/operations/tracing.html) via this configuration:

```
export OTEL_EXPORTER=otlphttp
export OTEL_EXPORTER_OTLP_TRACES_ENDPOINT="https://observe.gitlab.com/v3/6543/56106290/ingest/traces"
export OTEL_EXPORTER_OTLP_TRACES_HEADERS="PRIVATE-TOKEN=<PRAT>"

export TRACE_URL_PREFIX=https://gitlab.com/gitlab-com/gl-infra/tracing/-/tracing/
export TRACE_URL_FORMAT_UUID=true
```

## GraphQL

When making changes to the `gitlabgraphql` package, the client can be re-generated via:

```
go run github.com/Khan/genqlient
```
