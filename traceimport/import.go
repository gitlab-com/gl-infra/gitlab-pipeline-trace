package traceimport

import (
	"context"
	"fmt"
	"github.com/Khan/genqlient/graphql"
	"gitlab.com/gitlab-com/gl-infra/gitlab-pipeline-trace/gitlabgraphql"
	"gitlab.com/gitlab-org/api/client-go"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"io"
	"log"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"sync"
	"time"
)

// TODO: limit depth/duration/concurrency
func processPipeline(ctx context.Context, wg *sync.WaitGroup, client *gitlab.Client, graphqlClient graphql.Client, projectId any, pipelineId int, includeJobTraces bool) (string, error) {
	project, _, err := client.Projects.GetProject(projectId, &gitlab.GetProjectOptions{})
	if err != nil {
		return "", fmt.Errorf("failed to get project: %w", err)
	}

	// log.Printf("processPipeline: %v %v", project.PathWithNamespace, pipelineId)

	pipeline, _, err := client.Pipelines.GetPipeline(project.PathWithNamespace, pipelineId,
		gitlab.WithContext(ctx))
	if err != nil {
		return "", fmt.Errorf("failed to get pipeline: %w", err)
	}

	supportedStatuses := []string{"success", "failed", "canceled", "skipped", "manual", "running"}
	if !slices.Contains(supportedStatuses, pipeline.Status) {
		return "", fmt.Errorf("unsupported pipeline status: %s", pipeline.Status)
	}

	ctx, span := tracer.Start(ctx, fmt.Sprintf("%s: %s", project.PathWithNamespace, pipeline.Name),
		trace.WithTimestamp(*pipeline.StartedAt),
		trace.WithAttributes(
			attribute.Int("pipeline.id", pipeline.ID),
			attribute.String("pipeline.status", pipeline.Status),
			attribute.String("pipeline.web_url", pipeline.WebURL),
			attribute.String("project.path", project.PathWithNamespace),
			attribute.String("project.web_url", project.WebURL)))

	switch pipeline.Status {
	case "failed":
		span.SetStatus(codes.Error, pipeline.Status)
	case "success":
		span.SetStatus(codes.Ok, pipeline.Status)
	default:
		span.SetStatus(codes.Unset, pipeline.Status)
	}

	if pipeline.FinishedAt != nil {
		defer span.End(trace.WithTimestamp(*pipeline.FinishedAt))
	} else {
		defer span.End(trace.WithTimestamp(time.Now()))
	}

	err = processPipelineJobs(ctx, wg, client, graphqlClient, project.PathWithNamespace, pipelineId, includeJobTraces)
	if err != nil {
		return "", err
	}

	err = processPipelineBridges(ctx, wg, client, graphqlClient, project.PathWithNamespace, pipelineId, includeJobTraces)
	if err != nil {
		return "", err
	}

	return span.SpanContext().TraceID().String(), nil
}

func getPipelineJobNeedsViaGraphql(ctx context.Context, wg *sync.WaitGroup, graphqlClient graphql.Client, projectId any, pipelineId int) (map[string][]string, error) {
	var strProjectId string
	switch projectId := projectId.(type) {
	case int:
		strProjectId = strconv.Itoa(projectId)
	case string:
		strProjectId = projectId
	default:
		return nil, fmt.Errorf("invalid type for project id: %v", projectId)
	}

	resp, err := gitlabgraphql.GetPipelineJobNeeds(ctx, graphqlClient, strProjectId, fmt.Sprintf("gid://gitlab/Ci::Pipeline/%d", pipelineId))
	if err != nil {
		return nil, err
	}

	needs := make(map[string][]string)

	for _, edge1 := range resp.Project.Pipeline.Jobs.Edges {
		node1 := edge1.Node.Name
		for _, edge2 := range edge1.Node.Needs.Edges {
			node2 := edge2.Node.Name
			needs[node1] = append(needs[node1], node2)
		}
	}

	return needs, nil
}

func filterJobs(jobs []*gitlab.Job) []*gitlab.Job {
	var out []*gitlab.Job

	for _, job := range jobs {
		if job.StartedAt == nil {
			if job.Status != "skipped" && job.Status != "manual" && job.Status != "canceled" && job.Status != "created" {
				log.Printf("warning: unexpected job missing started_at, status: %s", job.Status)
			}
			continue
		}

		if job.FinishedAt == nil {
			if job.Status != "running" {
				log.Printf("warning: unexpected job missing finished_at, status: %s", job.Status)
			}
			continue
		}

		out = append(out, job)
	}

	return out
}

type jobGroup struct {
	name     string
	jobs     []*gitlab.Job
	earliest *gitlab.Job
	latest   *gitlab.Job
}

func groupJobs(jobs []*gitlab.Job) map[string]*jobGroup {
	re := regexp.MustCompile(`^(.+) (\d+)/(\d+)$`)

	groups := make(map[string]*jobGroup)
	for _, job := range jobs {
		groupName := ""

		match := re.FindStringSubmatch(job.Name)
		if len(match) > 1 {
			groupName = match[1]
		}

		if _, ok := groups[groupName]; !ok {
			groups[groupName] = &jobGroup{name: groupName}
		}

		group := groups[groupName]
		group.jobs = append(group.jobs, job)
		if group.earliest == nil || group.earliest.StartedAt.After(*job.StartedAt) {
			group.earliest = job
		}
		if group.latest == nil || group.latest.FinishedAt.Before(*job.FinishedAt) {
			group.latest = job
		}
	}

	return groups
}

func processPipelineJobs(ctx context.Context, wg *sync.WaitGroup, client *gitlab.Client, graphqlClient graphql.Client, projectId any, pipelineId int, includeJobTraces bool) error {
	needs, err := getPipelineJobNeedsViaGraphql(ctx, wg, graphqlClient, projectId, pipelineId)
	if err != nil {
		return err
	}

	var jobs []*gitlab.Job

	opt := &gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			OrderBy:    "id",
			Pagination: "keyset",
			PerPage:    100,
			Sort:       "asc",
		},
		IncludeRetried: gitlab.Ptr(true),
	}
	var options []gitlab.RequestOptionFunc
	options = append(options, gitlab.WithContext(ctx))
	for {
		jobsPage, resp, err := client.Jobs.ListPipelineJobs(projectId, pipelineId, opt, options...)
		if err != nil {
			return err
		}

		jobs = append(jobs, jobsPage...)

		if resp.NextLink == "" {
			break
		}
		options = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(resp.NextLink),
		}
	}

	// process jobs

	jobs = filterJobs(jobs)
	groups := groupJobs(jobs)

	for _, group := range groups {
		ctx2 := ctx
		var groupSpan trace.Span
		if group.name != "" {
			ctx2, groupSpan = tracer.Start(ctx, group.name,
				trace.WithTimestamp(*group.earliest.StartedAt))
		}

		for _, job := range group.jobs {
			ctx3, span := tracer.Start(ctx2, job.Name,
				trace.WithTimestamp(*job.StartedAt),
				trace.WithAttributes(
					attribute.Int("pipeline.id", job.Pipeline.ID),
					attribute.Int("job.id", job.ID),
					attribute.String("job.status", job.Status),
					attribute.String("job.web_url", job.WebURL),
					attribute.Float64("job.queued_duration_s", job.QueuedDuration),
					attribute.String("job.runner.description", job.Runner.Description)))
			switch job.Status {
			case "failed":
				span.SetAttributes(
					attribute.Bool("job.allow_failure", job.AllowFailure),
					attribute.String("job.failure_reason", job.FailureReason))
				if job.AllowFailure {
					span.SetStatus(codes.Ok, job.Status)
				} else {
					span.SetStatus(codes.Error, job.Status)
				}
			case "success":
				span.SetStatus(codes.Ok, job.Status)
			default:
				span.SetStatus(codes.Unset, job.Status)
			}

			if needs[job.Name] != nil {
				span.SetAttributes(attribute.String("needs", strings.Join(needs[job.Name], ", ")))
			}

			queuedDuration := time.Duration(job.QueuedDuration * float64(time.Second))
			if queuedDuration > time.Second {
				span.AddEvent("enqueued",
					trace.WithTimestamp(job.StartedAt.Add(-queuedDuration)),
					trace.WithAttributes(attribute.Float64("job.queued_duration_s", job.QueuedDuration)))
			}

			if job.Name == "wait:omnibus" || job.Name == "wait:cng" {
				awaitedPipelineURL, err := getAwaitedPipelineURL(ctx3, client, projectId, job.ID)
				if err != nil {
					return err
				}
				span.SetAttributes(attribute.String("job.awaited_pipeline_url", awaitedPipelineURL))
			}

			if includeJobTraces {
				err = processJobTrace(ctx3, client, span, projectId, job.ID)
				if err != nil {
					return err
				}
			}

			span.End(trace.WithTimestamp(*job.FinishedAt))
		}

		if groupSpan != nil {
			groupSpan.End(trace.WithTimestamp(*group.latest.FinishedAt))
		}
	}

	return nil
}

func getAwaitedPipelineURL(ctx context.Context, client *gitlab.Client, projectId any, jobId int) (string, error) {
	traceReader, _, err := client.Jobs.GetTraceFile(projectId, jobId,
		gitlab.WithContext(ctx))
	if err != nil {
		return "", err
	}

	buf := make([]byte, traceReader.Size())
	_, err = io.ReadFull(traceReader, buf)
	if err != nil {
		return "", err
	}

	re := regexp.MustCompile(`Checking status of pipeline -- {pipeline: "(.+)"}`)
	match := re.FindStringSubmatch(string(buf))
	if len(match) < 2 {
		return "", nil
	}

	return match[1], nil
}

type jobTraceSectionRecord struct {
	timestamp int
	name      string
}

func processJobTrace(ctx context.Context, client *gitlab.Client, span trace.Span, projectId any, jobId int) error {
	traceReader, _, err := client.Jobs.GetTraceFile(projectId, jobId,
		gitlab.WithContext(ctx))
	if err != nil {
		return err
	}

	buf := make([]byte, traceReader.Size())
	_, err = io.ReadFull(traceReader, buf)
	if err != nil {
		return err
	}

	re := regexp.MustCompile(`(section_start|section_end):(\d+):(\w+)(?:\[.+\]?)?\r`)
	matches := re.FindAllStringSubmatch(string(buf), 1024)

	var stack []jobTraceSectionRecord

	for _, match := range matches {
		type_ := match[1]
		timestamp, err := strconv.Atoi(match[2])
		if err != nil {
			return err
		}
		sectionName := match[3]

		record := jobTraceSectionRecord{
			timestamp: timestamp,
			name:      sectionName,
		}

		if type_ == "section_start" {
			stack = append(stack, record)
			continue
		}

		for len(stack) > 0 && record.name != stack[len(stack)-1].name {
			//log.Printf("warning: job trace for pipeline %v %d contains missing section end marker for section %s", projectId, jobId, startRecord.name)
			startRecord := stack[len(stack)-1]
			stack = stack[:len(stack)-1]

			if record.timestamp-startRecord.timestamp <= 10 {
				continue
			}

			span.AddEvent(startRecord.name,
				trace.WithTimestamp(time.Unix(int64(record.timestamp), 0)),
				trace.WithAttributes(attribute.Int("section.duration_s", record.timestamp-startRecord.timestamp)))
		}

		if len(stack) == 0 {
			log.Printf("warning: job trace for pipeline %v %d contains surplus end marker for section %s", projectId, jobId, record.name)
			return nil
		}

		startRecord := stack[len(stack)-1]
		stack = stack[:len(stack)-1]

		if record.timestamp-startRecord.timestamp <= 10 {
			continue
		}

		span.AddEvent(record.name,
			trace.WithTimestamp(time.Unix(int64(record.timestamp), 0)),
			trace.WithAttributes(attribute.Int("section.duration_s", record.timestamp-startRecord.timestamp)))
	}

	return nil
}

func processPipelineBridges(ctx context.Context, wg *sync.WaitGroup, client *gitlab.Client, graphqlClient graphql.Client, projectId any, pipelineId int, includeJobTraces bool) error {
	opt := &gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			OrderBy:    "id",
			Pagination: "keyset",
			PerPage:    100,
			Sort:       "asc",
		},
		IncludeRetried: gitlab.Ptr(true),
	}
	var options []gitlab.RequestOptionFunc
	options = append(options, gitlab.WithContext(ctx))
	for {
		jobs, resp, err := client.Jobs.ListPipelineBridges(projectId, pipelineId, opt, options...)
		if err != nil {
			return err
		}

		for _, job := range jobs {
			if job.DownstreamPipeline == nil {
				continue
			}
			wg.Add(1)
			go func(job gitlab.Bridge) {
				defer wg.Done()

				ctx2, span := tracer.Start(ctx, job.Name,
					trace.WithTimestamp(*job.StartedAt),
					trace.WithAttributes(
						attribute.Int("pipeline.id", job.Pipeline.ID),
						attribute.Int("job.id", job.ID),
						attribute.String("job.web_url", job.WebURL)))

				_, err = processPipeline(ctx2, wg, client, graphqlClient, job.DownstreamPipeline.ProjectID, job.DownstreamPipeline.ID, includeJobTraces)
				if err != nil {
					panic(err)
				}

				if job.FinishedAt != nil {
					span.End(trace.WithTimestamp(*job.FinishedAt))
				} else {
					span.SetStatus(codes.Unset, "still running")
					span.End(trace.WithTimestamp(time.Now()))
				}
			}(*job)
		}

		if resp.NextLink == "" {
			break
		}
		options = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(resp.NextLink),
		}
	}

	return nil
}

func ProcessPipeline(ctx context.Context, client *gitlab.Client, graphqlClient graphql.Client, projectId string, pipelineId int, includeJobTraces bool) (string, error) {
	var wg sync.WaitGroup

	traceId, err := processPipeline(ctx, &wg, client, graphqlClient, projectId, pipelineId, includeJobTraces)
	if err != nil {
		return "", fmt.Errorf("failed to process pipeline: %w", err)
	}

	wg.Wait()

	return traceId, nil
}
