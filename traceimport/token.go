package traceimport

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"path"
)

type GlabHostConfig struct {
	Token string `yaml:"token"`
}

type GlabConfig struct {
	Hosts map[string]GlabHostConfig `yaml:"hosts"`
}

func GetGitlabToken(gitlabHost string) (string, error) {
	gitlabToken := os.Getenv("GITLAB_TOKEN")

	if gitlabToken == "" {
		glabConfigPath := path.Join(os.Getenv("HOME"), ".config", "glab-cli", "config.yml")

		buf, err := os.ReadFile(glabConfigPath)
		if err != nil {
			return "", fmt.Errorf("failed to read glab config file: %w", err)
		}

		glabConfig := &GlabConfig{}

		err = yaml.Unmarshal(buf, &glabConfig)
		if err != nil {
			return "", fmt.Errorf("failed to parse yaml: %v: %w", glabConfigPath, err)
		}

		glabHostConfig, ok := glabConfig.Hosts[gitlabHost]
		if !ok {
			return "", fmt.Errorf("glab config missing entry for host: %v: %v", glabConfigPath, gitlabHost)
		}

		gitlabToken = glabHostConfig.Token
	}

	return gitlabToken, nil
}
