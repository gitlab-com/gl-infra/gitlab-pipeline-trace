package traceimport

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/gitlab-org/api/client-go"
	"os"
)

func RunListPipelines(ctx context.Context, client *gitlab.Client, projectId string, pipelineName string, limit int, formatUrl bool) error {
	seen := 0

	opt := &gitlab.ListProjectPipelinesOptions{
		ListOptions: gitlab.ListOptions{
			OrderBy:    "id",
			Pagination: "keyset",
			PerPage:    100,
			Sort:       "desc",
		},
		Status: gitlab.Ptr(gitlab.Success),
	}
	if pipelineName != "" {
		opt.Name = gitlab.Ptr(pipelineName)
	}

	var options []gitlab.RequestOptionFunc
	options = append(options, gitlab.WithContext(ctx))
	for {
		pipelines, resp, err := client.Pipelines.ListProjectPipelines(projectId, opt, options...)
		if err != nil {
			return err
		}

		for _, pipeline := range pipelines {
			if seen >= limit {
				break
			}
			seen++
			if formatUrl {
				fmt.Println(pipeline.WebURL)
			} else {
				if err := json.NewEncoder(os.Stdout).Encode(pipeline); err != nil {
					return err
				}
			}
		}

		if seen >= limit {
			break
		}

		if resp.NextLink == "" {
			break
		}
		options = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(resp.NextLink),
		}
	}

	return nil
}
