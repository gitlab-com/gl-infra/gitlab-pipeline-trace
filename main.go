package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/Khan/genqlient/graphql"
	"github.com/alecthomas/kingpin/v2"
	"github.com/google/uuid"
	"github.com/pkg/profile"
	"gitlab.com/gitlab-com/gl-infra/gitlab-pipeline-trace/traceimport"
	"gitlab.com/gitlab-org/api/client-go"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
)

var (
	app = kingpin.New("gitlab-pipeline-trace", "GitLab pipeline to otel trace")

	importCmd                   = app.Command("import", "Import a trace")
	importCmdPipelineUrl        = importCmd.Arg("pipeline-url", "Pipeline URL").Required().String()
	importCmdTraceUrlPrefix     = importCmd.Flag("trace-url-prefix", "Prefix for trace URL").Default("http://localhost:16686/trace/").Envar("TRACE_URL_PREFIX").String()
	importCmdTraceUrlFormatUuid = importCmd.Flag("trace-url-format-uuid", "Format ID in trace URL as UUID").Default("false").Envar("TRACE_URL_FORMAT_UUID").Bool()
	importCmdIncludeJobTraces   = importCmd.Flag("include-job-traces", "Parse job traces for events (slow)").Default("false").Bool()

	listCmd             = app.Command("list", "List recent pipelines")
	listCmdProjectUrl   = listCmd.Arg("project-url", "Project URL").Required().String()
	listCmdPipelineName = listCmd.Flag("pipeline-name", "Pipeline name to filter for").String()
	listCmdLimit        = listCmd.Flag("limit", "Limit output to this many entries").Default("200").Int()
	listCmdFormatUrl    = listCmd.Flag("url", "Output just the URL").Bool()
)

type authedTransport struct {
	key     string
	wrapped http.RoundTripper
}

func (t *authedTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Authorization", "bearer "+t.key)
	return t.wrapped.RoundTrip(req)
}

func runImport(ctx context.Context) error {
	pipelineURL := *importCmdPipelineUrl
	u, err := url.Parse(pipelineURL)
	if err != nil {
		return fmt.Errorf("failed to parse URL: %w", err)
	}

	re := regexp.MustCompile(`^/(.+)/-/pipelines/(\d+)$`)
	matches := re.FindStringSubmatch(u.Path)
	if len(matches) == 0 {
		return fmt.Errorf("URL must be a pipeline URL: %s", pipelineURL)
	}

	gitlabToken, err := traceimport.GetGitlabToken(u.Host)
	if err != nil {
		return err
	}
	if gitlabToken == "" {
		return errors.New("please provide GITLAB_TOKEN or configure glab")
	}

	client, err := gitlab.NewClient(
		gitlabToken,
		gitlab.WithBaseURL(fmt.Sprintf("%s://%s", u.Scheme, u.Host)),
	)
	if err != nil {
		return fmt.Errorf("failed to create client: %w", err)
	}

	graphqlClient := graphql.NewClient(
		fmt.Sprintf("%s://%s/api/graphql", u.Scheme, u.Host),
		&http.Client{
			Transport: &authedTransport{
				key:     gitlabToken,
				wrapped: http.DefaultTransport,
			},
		},
	)

	projectId := matches[1]
	pipelineId, err := strconv.Atoi(matches[2])
	if err != nil {
		return fmt.Errorf("failed to parse pipeline id: %w", err)
	}

	traceId, err := traceimport.ProcessPipeline(ctx, client, graphqlClient, projectId, pipelineId, *importCmdIncludeJobTraces)
	if err != nil {
		return err
	}

	if *importCmdTraceUrlFormatUuid {
		traceUuid, err := uuid.Parse(traceId)
		if err != nil {
			return err
		}
		traceId = traceUuid.String()
	}

	fmt.Printf("%s%s\n", *importCmdTraceUrlPrefix, traceId)

	return nil
}

func runList(ctx context.Context) error {
	pipelineURL := *listCmdProjectUrl
	u, err := url.Parse(pipelineURL)
	if err != nil {
		return fmt.Errorf("failed to parse URL: %w", err)
	}

	if len(u.Path) == 0 {
		return fmt.Errorf("project url requires path containing project")
	}

	gitlabToken, err := traceimport.GetGitlabToken(u.Host)
	if err != nil {
		return err
	}
	if gitlabToken == "" {
		return errors.New("please provide GITLAB_TOKEN or configure glab")
	}

	client, err := gitlab.NewClient(
		gitlabToken,
		gitlab.WithBaseURL(fmt.Sprintf("%s://%s", u.Scheme, u.Host)),
	)
	if err != nil {
		return fmt.Errorf("failed to create client: %w", err)
	}

	// strip leading /
	projectId := u.Path[1:]
	return traceimport.RunListPipelines(ctx, client, projectId, *listCmdPipelineName, *listCmdLimit, *listCmdFormatUrl)
}

func main() {
	switch os.Getenv("PROFILE") {
	case "cpu":
		defer profile.Start(profile.CPUProfile).Stop()
	case "memory":
		defer profile.Start(profile.MemProfile).Stop()
	case "block":
		defer profile.Start(profile.BlockProfile).Stop()
	case "goroutine":
		defer profile.Start(profile.GoroutineProfile).Stop()
	case "trace":
		defer profile.Start(profile.TraceProfile).Stop()
	}

	ctx := context.Background()

	otelShutdown, err := traceimport.SetupOTelSDK(ctx)
	if err != nil {
		log.Fatalf("failed to setup otel sdk: %v", err)
	}
	defer func() {
		err = otelShutdown(context.Background())
		if err != nil {
			log.Fatalf("failed to shutdown otel sdk: %v", err)
		}
	}()

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case importCmd.FullCommand():
		err = runImport(ctx)
		if err != nil {
			log.Print(err)
		}

	case listCmd.FullCommand():
		err = runList(ctx)
		if err != nil {
			log.Print(err)
		}
	}
}
